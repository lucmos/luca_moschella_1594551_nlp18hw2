import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt

import Utils
import wsd_evaluator
from wsd_loader import Loader
from babelnet_importer import BabelMap

INPUT_FILE = "./10-06-2018_12h-32m_dev_predictions(both).json"
PICS_FOLDER = "./pics/"

CMAP = sns.light_palette((210, 90, 60), n_colors=64, input="husl")


def plot_prfs_matrix(label, fmeasure, correct, disambiguated, total, ftot, ctot, dtot, ttot, title):
    """
    Plots the performances.
    """
    sns.set(style="white")
    data = pd.DataFrame({"F-measure": fmeasure, "Correct": correct, "Disambiguated": disambiguated, "Total": total},
                        index=label, columns=["F-measure", "Correct", "Disambiguated", "Total"])
    avg = pd.DataFrame({"F-measure": ftot, "Correct": ctot, "Disambiguated": dtot, "Total": ttot}, index=["Overall"], columns=["F-measure", "Correct", "Disambiguated", "Total"])

    f, ax = plt.subplots()
    data = data.append(avg)
    annot = data.copy()
    data.loc["Overall"] = data.loc["Overall"] * 0
    data = data.apply(lambda x: Utils.min_max_normalization(x), axis=0)

    sns.heatmap(data, cbar=False, cmap=CMAP, annot=annot, fmt="g", linewidths=2.5)
    plt.title(title)
    plt.tight_layout()
    plt.savefig(os.path.join(PICS_FOLDER, "{}.png".format("_".join(title.split()).lower())), dpi=500)
    plt.close(f)


def listfy(semcors, fmeasure):
    return [fmeasure[x] for x in semcors]


if __name__ == '__main__':
    """
    Reads the results and makes the plot.
    """
    evaluator = wsd_evaluator.Evaluator()
    semcors = evaluator.semcors

    most_common_pred, avg_pred = Utils.load_json(INPUT_FILE)

    _, _, fmeasure, correct, disa, tot = evaluator.eval_by_semcor(most_common_pred)
    fmeasure_common = listfy(semcors, fmeasure)
    correct_common = listfy(semcors, correct)
    disa_common = listfy(semcors, disa)
    tot_common = listfy(semcors, tot)

    _, _, fmeasure_tot, correct_tot, disa_tot, tot_tot = evaluator.eval(most_common_pred)

    print(correct_tot, disa_tot, tot_tot)

    plot_prfs_matrix(semcors, fmeasure_common, correct_common, disa_common, tot_common, fmeasure_tot, correct_tot, disa_tot, tot_tot, "Predictions using domain words")

    _, _, fmeasure, correct, disa, tot = evaluator.eval_by_semcor(avg_pred)
    fmeasure_common = listfy(semcors, fmeasure)
    correct_common = listfy(semcors, correct)
    disa_common = listfy(semcors, disa)
    tot_common = listfy(semcors, tot)

    _, _, fmeasure_tot, correct_tot, disa_tot, tot_tot = evaluator.eval(avg_pred)

    print(correct_tot, disa_tot, tot_tot)

    plot_prfs_matrix(semcors, fmeasure_common, correct_common, disa_common, tot_common, fmeasure_tot, correct_tot,
                     disa_tot, tot_tot, "Predictions using sentence average")


    _, _, fmeasure, correct, disa, tot = evaluator.eval_by_semcor(evaluator.mfs.baseline(evaluator.mfs.predictor()))
    fmeasure_common = listfy(semcors, fmeasure)
    correct_common = listfy(semcors, correct)
    disa_common = listfy(semcors, disa)
    tot_common = listfy(semcors, tot)

    _, _, fmeasure_tot, correct_tot, disa_tot, tot_tot = evaluator.eval(evaluator.mfs.baseline(evaluator.mfs.predictor()))

    print(correct_tot, disa_tot, tot_tot)

    plot_prfs_matrix(semcors, fmeasure_common, correct_common, disa_common, tot_common, fmeasure_tot, correct_tot,
                     disa_tot, tot_tot, "Predictions using MFS (train -> wordnet -> babelnet)")
