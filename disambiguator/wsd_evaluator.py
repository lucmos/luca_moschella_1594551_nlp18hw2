import pandas as pd

from constants import WORDNET, BABELNET, ALL_BABELNET
from wsd_loader import Loader, DEV, TRAIN, ID, WORD, LEMMA, POS, TEST
from wsd_mfs import MFS
from babelnet_importer import BabelMap


class Evaluator:
    """
    Evaluates the performances of a prediction system
    """

    def __init__(self):
        self.mfs = MFS()
        self.loader = Loader.get_instance()

        self.total_predictions = {x: len(self.loader.id2synset[x]) for x in [TRAIN, DEV]}

        self.semcors = sorted(list(set(Loader.get_semcor(DEV, x) for x in self.loader.id2synset[DEV])))
        self.y_true_by_semcors = {x: {y: self.loader.id2synset[DEV][y] for y in self.loader.id2synset[DEV]
                                      if x == Loader.get_semcor(DEV, y)} for x in self.semcors}
        self.total_predictions_by_semcors = {x: len(self.y_true_by_semcors[x]) for x in self.semcors}

    def eval(self, y_pred, dataset_name=DEV):
        """
        Perform the evaluation of the predictions over the full dataset_name
        :param y_pred: a map <id:synset> of the predictions performed
        :param dataset_name: the dataset against which the predictions must be evaluated
        :return: the precision, recall and f-measure
        """
        y_true = self.loader.id2synset[dataset_name]
        total_prediction = self.total_predictions[dataset_name]
        performed_prediction = len(y_pred)
        correct_prediction = sum(1 for x in y_pred if y_pred[x] == y_true[x])

        precision = correct_prediction / performed_prediction
        recall = correct_prediction / total_prediction
        fmeasure = (2 * precision * recall) / (precision + recall)

        d = pd.DataFrame({"all": [fmeasure, precision, recall, correct_prediction, performed_prediction,
                                  self.total_predictions[dataset_name] - performed_prediction,
                                  self.total_predictions[dataset_name]]},
                         index=["f-measure", "precision", "recall", "correct", "disambiguated", "not_disambiguated",
                                "total"])

        print(d.transpose().to_string())
        return precision, recall, fmeasure, correct_prediction, performed_prediction, total_prediction

    def eval_by_semcor(self, y_pred):
        """
        Perform the evaluation of the predictions, divided by semeval
        :param y_pred: a map <id:synset> of the predictions performed
        :return: the precision, recall and f-measure
        """
        performed_prediction = {x: 0 for x in self.semcors}
        correct_prediction = {x: 0 for x in self.semcors}

        for idd, pred in y_pred.items():
            sem = Loader.get_semcor(DEV, idd)

            performed_prediction[sem] += 1
            if pred == self.y_true_by_semcors[sem][idd]:
                correct_prediction[sem] += 1

        precision = {x: correct_prediction[x] / performed_prediction[x] if performed_prediction[x] else 0 for x in
                     self.semcors}
        recall = {x: correct_prediction[x] / self.total_predictions_by_semcors[x] for x in self.semcors}
        fmeasure = {x: (2 * precision[x] * recall[x]) / (precision[x] + recall[x]) if precision[x] + recall[x] else None
                    for x in self.semcors}

        d = pd.DataFrame({x: [fmeasure[x], precision[x], recall[x], (correct_prediction[x]), (performed_prediction[x]),
                              (self.total_predictions_by_semcors[x]) - performed_prediction[x],
                              (self.total_predictions_by_semcors[x])] for x in self.semcors},
                         index=["f-measure", "precision", "recall", "correct", "disambiguated", "not_disambiguated",
                                "total"])

        print(d.transpose().to_string())
        return precision, recall, fmeasure, correct_prediction, performed_prediction, self.total_predictions_by_semcors


if __name__ == '__main__':
    """
    Some performance testing with the baseline.
    """

    a = Evaluator()

    M = {"word": a.mfs.with_word,
         "word_pos": a.mfs.with_wordpos,
         "lemma": a.mfs.with_lemma,
         "lemma_pos": a.mfs.with_lemmapos}
    print("BASELINE (lemma_pos) in semcor, as backoff (lemma_pos) in Wordnet, Babelnet, All_babelnet")
    a.eval_by_semcor(a.mfs.baseline(a.mfs.predictor_template([M["lemma_pos"], M["lemma"], M["lemma"], M["lemma"]],
                                                             [None, WORDNET, BABELNET, ALL_BABELNET])))
    a.eval(a.mfs.baseline(a.mfs.predictor_template([M["lemma_pos"], M["lemma"], M["lemma"], M["lemma"]],
                                                   [None, WORDNET, BABELNET, ALL_BABELNET])))
    print()

    print("BASELINE (word_pos) in semcor, as backoff (lemma_pos) in Wordnet, Babelnet, All_babelnet")
    a.eval_by_semcor(a.mfs.baseline(a.mfs.predictor_template([M["word_pos"], M["lemma"], M["lemma"], M["lemma"]],
                                                             [None, WORDNET, BABELNET, ALL_BABELNET])))
    a.eval(a.mfs.baseline(a.mfs.predictor_template([M["word_pos"], M["lemma"], M["lemma"], M["lemma"]],
                                                   [None, WORDNET, BABELNET, ALL_BABELNET])))
    print()

    print("BASELINE (lemma_pos) in Babelnet")
    a.eval_by_semcor(a.mfs.baseline(a.mfs.predictor_template([M["lemma_pos"]], [BABELNET])))
    a.eval(a.mfs.baseline(a.mfs.predictor_template([M["lemma_pos"]], [BABELNET])))
    print()

    print("Predictin only monosemous words with knowledge base")
    a.eval_by_semcor(a.mfs.baseline_monosemous())
    print()
