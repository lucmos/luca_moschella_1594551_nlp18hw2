"""
Common constants used in the project.
"""

####### COMMON KEYS #####
BABELNET = "babelnet"
WORDNET = "wordnet"
ALL_BABELNET = "all_babelnet"

TRAIN = "train"
DEV = "dev"
TEST = "test"

NOUN = "NOUN"
VERB = "VERB"
ADJ = "ADJ"
ADV = "ADV"

PRED = "prob"
PROBA = "proba"

L_W2MOST_COMMON = "lemma_word_2_most_common_synset"
DOMAIN2SYNSETS = "domain_2_distance_2_context_words"
DOMAIN2WORDS = "domain2words"

GENERATED_JSON_FIELDS = [L_W2MOST_COMMON, DOMAIN2SYNSETS]


WORDNET_POINTERS = "wn_pointers"
BABELNET_POINTERS = "bn_pointers"

####### EMBEDDINGS ######
GOOGLE_EMBEDDINGS = "./embeddings/GoogleNews-vectors-negative300.bin"
GOOGLE_DICTIONARY = "./embeddings/google_dictionary.pickle"

####### CLASSIFIER ######

# directories
TRAIN_DIR_CLASSIFIER = "dataset/DATA/TRAIN"
DEV_DIR_CLASSIFIER = "dataset/DATA/DEV"
TEST_DIR_CLASSIFIER = "dataset/DATA/TEST"

# cache
CACHE_FOLDER_CLASSIFIER = "./cache/classifier"

DATAMANAGER_CACHE_FILE = "datamanager.pickle"
DATAMANAGER_TRAIN_DATA = "datamanager_train_data.pickle"
DATAMANAGER_DEV_DATA = "datamanager_dev_data.pickle"


DOMAIN_IMPORTANCE_CACHE_FILE = "domain_importance.pickle"
FILE_EMBEDDINGS_CACHE = "file_embeddings.pickle"
FILE_FEATURES_VECTORS_CACHE = "file_features_vector.pickle"

TRAINING_DATA_CACHE = "training_data_cache.pickle"


# output
CLASSIFIER_TRAINED_MODEL = "./classifier/xgboost.pickle"
RUN_INFORMATION_FILE = "./classifier/run_information.pickle"


####### DISAMBIGUATOR ######

####### loader

# datsets
TRAIN_DATASET_WSD = "./dataset/WSD/semcor.data.xml"
DEV_DATASET_WSD = "./dataset/WSD/ALL.data.xml"
TEST_DATASET_AND_BINDIGS_WSD = "./dataset/WSD/test_data.txt"

TRAIN_BINDING_WSD = "./dataset/WSD/semcor.gold.key.bnids.txt"
DEV_BINDING_WSD = "./dataset/WSD/ALL.gold.key.bnids.txt"

# cache
CACHE_FILE_LOADER = "./cache/disambiguator/loader.pickle"
CACHE_FILE_BABELMAP = "./cache/disambiguator/babelmap.pickle"
CACHE_FILE_PROVIDER = "./cache/disambiguator/provider.pickle"

# predictions
CACHE_FILE_DATASET_DOMAIN_PREDICTIONS = "./cache/disambiguator/domain_predictions.pickle"

####### babelnet

EXPORTED_JSON_PYTHON = "./babelnet/exported_python.json"
EXPORTED_JSON_BABELNET_COMMON = "./babelnet/exported_babelnet_common.json"
EXPORTED_JSON_BABELNET_ALL_POINTERS = "./babelnet/exported_babelnet_all_pointers.json"
EXPORTED_JSON_BABELNET_WORDNET_PONTERS = "./babelnet/exported_babelnet_wordnet_pointers.json"
EXPORTED_JSON_BABELNET_DOMAINS = "./babelnet/exported_babelnet_domain_words.json"