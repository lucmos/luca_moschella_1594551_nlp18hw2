import os
import Utils

if __name__ == '__main__':
    """
    Script to convert pickle in json
    """
    to_convert = ["./classifier/result_training_summary_18-05-2018_03h-49m.pickle",
                  "./classifier/best_iter_result_training_summary_18-05-2018_13h-58m.pickle"]

    for x in to_convert:
        pickle_obj = Utils.load_pickle(x)

        result = {}
        if "classifier_params" in  pickle_obj:
            result["classifier_params"] = pickle_obj["classifier_params"]
        result["classification_report"] = pickle_obj["classification_report"]
        result["precision_average"] = pickle_obj["precision_average"]
        result["recall_average"] = pickle_obj["recall_average"]
        result["f-measure_average"] = pickle_obj["f-measure_average"]

        dire = os.path.dirname(x)
        name_ext = os.path.basename(x)
        name = os.path.splitext(name_ext)[0]
        json_path = os.path.join(dire, name + ".json")
        Utils.save_json(result, json_path, override=True)
