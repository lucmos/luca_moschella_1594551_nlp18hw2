import Utils
from wsd_loader import Loader, ID , WORD, LEMMA, POS
from domain_classifier import Metrics
from Chronometer import Chrono
from constants import *
import tokenizer
from nltk.corpus import wordnet as wn

class JsonGen:
    """
    Used to export the words that need to be processed in Java using the babelnet API
    """
    @staticmethod
    def export_json():
        j = JsonGen()
        Utils.save_json(j.json_data, EXPORTED_JSON_PYTHON, override=True)

    def __init__(self):
        self.loader = Loader.get_instance()
        self.metrics = Metrics(load_only_run_information=True, compute_files_vectors=False)
        self.json_data = {x: {} for x in GENERATED_JSON_FIELDS}
        self._storage_cache = {}

        # Load Google's dictionary.
        c = Chrono("Loading google dictionary...")
        self.word_dict = Utils.load_pickle(GOOGLE_DICTIONARY)
        c.millis()

        self._get_domains()
        self._get_used_words_lemmas()

    def _get_domains(self):
        self.json_data[DOMAIN2SYNSETS] = self.metrics.run_info.list_domains

    def _get_used_words_lemmas(self):
        """
        Save the words or lemma used
        """
        words = set()

        for dataset_name in [TRAIN, DEV, TEST]:
            for d, document in enumerate(self.loader.doc_sentences[dataset_name][ID]):
                for s, sentence in enumerate(document):
                    for i, idd in enumerate(sentence):
                        if not idd:
                            continue

                        word = self.loader.doc_sentences[dataset_name][WORD][d][s][i]
                        lemma = self.loader.doc_sentences[dataset_name][LEMMA][d][s][i]

                        words.add(word)
                        words.add(lemma)

                        words.update(y for y in tokenizer.tokenize_line_google(word, self.word_dict) if y)
                        words.update(y for y in tokenizer.tokenize_line_google(lemma, self.word_dict) if y)

            for doc in self.loader.doc_sentences_word_restored[dataset_name]:
                for sentence in doc:
                    words.update(x for x in tokenizer.tokenize_line_google(sentence, self.word_dict))

        self.json_data[L_W2MOST_COMMON] = {x: {"wordnet": self.get_wordnet_synset(x)} for x in sorted(words)}

    def get_wordnet_synset(self, word):
        """
        Use word to get the most common synset. It will be converted by the babelnet API.

        The most common sense of Babelnet and Wordnet of a word present in Wordnet may differ
        :param word: the word
        :return: the mfs of the word
        """
        if word in self._storage_cache:
            return self._storage_cache[word]
        most_frequent_syn = {}

        # the suffix changes between wordnet and babelnet!
        for p, name, suff in zip([wn.VERB, wn.NOUN, wn.ADJ, wn.ADV], ["VERB", "NOUN", "ADJ", "ADV"], ["v", "n", "a", "r"]):

            wn_syn_list = wn.synsets(word, pos=p)
            if not wn_syn_list:
                most_frequent_syn[name] = None
            else:
                wn_syn = wn_syn_list[0]
                wn_id = "wn:" + str(wn_syn.offset()).zfill(8) + suff
                most_frequent_syn[name] = wn_id

        self._storage_cache[word] = most_frequent_syn
        return most_frequent_syn


if __name__ == '__main__':
    JsonGen.export_json()
