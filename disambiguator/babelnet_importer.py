from constants import *
from Chronometer import Chrono
import Utils


class BabelMap:
    """
    Imports the output of the Babelnet Java API
    """

    instance = None

    @staticmethod
    def get_instance(renew=False):
        """
        Singleton pattern, with cache and possibility to renew it
        :param renew: Whether to renew the cache
        :return: an instance of this class
        """

        if BabelMap.instance and not renew:
            return BabelMap.instance

        c = Chrono("Loading babelnet mappings...")
        cache = None
        if renew:
            BabelMap.instance = BabelMap()
        else:
            cache = Utils.load_pickle(CACHE_FILE_BABELMAP)
            BabelMap.instance = cache if cache else BabelMap()

        if not cache:
            c.millis("loaded from json")
            Utils.save_pickle(BabelMap.instance, CACHE_FILE_BABELMAP, override=renew)
        else:
            c.millis("cached")
        return BabelMap.instance

    def __init__(self):
        data_common = Utils.load_json(EXPORTED_JSON_BABELNET_COMMON)
        self.word2most_common = data_common[L_W2MOST_COMMON]

        self.domain2context = {}

        data_all_pointer = Utils.load_json(EXPORTED_JSON_BABELNET_ALL_POINTERS)
        self.domain2context[BABELNET_POINTERS] = data_all_pointer[DOMAIN2SYNSETS]

        data_wordnet_pointer = Utils.load_json(EXPORTED_JSON_BABELNET_WORDNET_PONTERS)
        self.domain2context[WORDNET_POINTERS] = data_wordnet_pointer[DOMAIN2SYNSETS]

        data_domains = Utils.load_json(EXPORTED_JSON_BABELNET_DOMAINS)
        self.domain2context[DOMAIN2WORDS] = data_domains[DOMAIN2WORDS]

        self.domains_name = sorted(self.domain2context.keys())

    @staticmethod
    def get_words_within(distance, domains2contexts):
        """
        Deprecated. Use the words domains computed directly from the babelnet's domains.

        Groups the words in domains2contexts that are at distance at most distance

        :param distance: the max distance
        :param domains2contexts: the map {domains: {distance: [words..]}}
        :return: the words {domains: [words...]} at distance at most "distance"
        """
        domain2contexts_within = {}
        for x in domains2contexts:
            domain2contexts_within[x] = set()
            for i in (str(x) for x in range(distance + 1)):
                domain2contexts_within[x].update(domains2contexts[x][i])
        return domain2contexts_within


if __name__ == '__main__':
    b = BabelMap.get_instance(renew=True)

    print("Number of words with common synsets {}".format(len(b.word2most_common)))
    print()

    print("ANIMALS -  ALL POINTERS")
    for i in sorted(b.domain2context[BABELNET_POINTERS]["ANIMALS"].keys()):
        print("Number of words in context at distance {}:".format(i), len(b.domain2context[BABELNET_POINTERS]["ANIMALS"][i]))
        print("Number of words in context within distance {}:".format(i), len(b.get_words_within(int(i), b.domain2context[BABELNET_POINTERS])["ANIMALS"]))
        print()

    print("ANIMALS -  WORDNET POINTERS")
    for i in sorted(b.domain2context[WORDNET_POINTERS]["ANIMALS"].keys()):
        print("Number of words in context at distance {}:".format(i), len(b.domain2context[WORDNET_POINTERS]["ANIMALS"][i]))
        print("Number of words in context within distance {}:".format(i), len(b.get_words_within(int(i), b.domain2context[WORDNET_POINTERS])["ANIMALS"]))
        print()

    total_words = 0
    for x in b.domain2context[DOMAIN2WORDS]:
        count = len(b.domain2context[DOMAIN2WORDS][x])
        total_words += count
        print("{}: {}\t---\t{}".format(x, count, b.domain2context[DOMAIN2WORDS][x][:50]))
    print("\nNumber of total words {}".format(total_words))

