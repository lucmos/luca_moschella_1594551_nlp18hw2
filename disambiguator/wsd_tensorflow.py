from collections import Counter
import numpy as np
import time
import tqdm

import tensorflow as tf

import Utils
from wsd_loader import Loader, ID, LEMMA, WORD, POS
from babelnet_importer import BabelMap
import domain_classifier
from wsd_tensorflow_batch import Batch
from wsd_tensorflow_provider import Provider
from wsd_mfs import MFS
import wsd_evaluator
from constants import *

RESTORE_MODDEL = True
SAVE_MODEL = False
PERFOM_TRAINING = True

EMBEDDINGS_DIM = 300
BATCH_SIZE = 16

ITEMS_IN_TRAIN_SET = 147264
NUMBER_OF_EPOCHS = 150
TRAINING_STEPS = NUMBER_OF_EPOCHS * int(ITEMS_IN_TRAIN_SET / BATCH_SIZE + 1)  # 500000
SAMPLE_SIZE = 64

PREDICTIONS_REPETITIONS = 1001

DEFAULT_TENSORBOARD_FOLDER = "./tensorboard/"
TENSORFLOW_MODEL = "./tensorflow_model/model.ckpt"

INPUT = 604
HIDDEN_LAYER_1 = 512
HIDDEN_LAYER_2 = 512

LEARNING_RATE = 0.0001

PRINT_STEPS = 10000

TIME_ID = time.strftime("%d-%m-%Y_%Hh-%Mm")
OUTPUT_TEST_FILE = "{}_test_answers.tsv".format(TIME_ID)
DEV_PREDICTIONS = "{}_dev_predictions(both).json".format(TIME_ID)

print(HIDDEN_LAYER_1, HIDDEN_LAYER_2, LEARNING_RATE, BATCH_SIZE, SAMPLE_SIZE, NUMBER_OF_EPOCHS, TRAINING_STEPS)


class WSD:
    """
    Train a neural network and perform the predictions.
    The constants SAVE_MODEL, RESTORE_MODEL, TRAIN_MODEL are used to decide which actions must be done.
    """

    def __init__(self):
        self.provider = Provider.get_instance()
        self.batcher = Batch()
        self.mfs = MFS()
        self.evaluator = wsd_evaluator.Evaluator()

        self.vocab_size = len(self.provider.dictionary_list)

        self.semcor_data = self.provider.doc_sentence_tokenized_numeric_flatted_padded
        self.semcor_mask = self.provider.doc_sentence_tokenized_numeric_flatted_mask
        self.semcor_size = len(self.semcor_data)

        self.semcor_average_sentence = np.ndarray(shape=(len(self.semcor_data), EMBEDDINGS_DIM))
        for i_sen, sentence in enumerate(self.semcor_data):
            e = [self.provider.embeddings_list_used[iw] for iw, w in enumerate(sentence) if self.semcor_mask[i_sen][iw]]
            if e:
                self.semcor_average_sentence[i_sen] = np.average(e, axis=0)
            else:
                self.semcor_average_sentence[i_sen] = np.zeros(EMBEDDINGS_DIM)

        self.max_sentence_size = self.provider.max_sentence_len

        self.words_in_domain = self.provider.words_in_domaincontext_numeric_padded
        self.words_in_domain_mask = self.provider.words_in_domaincontext_numeric_mask
        self.number_domains = len(self.words_in_domain)
        self.max_domain_size = self.provider.max_domain_words_len

        self.domains_name = self.provider.domains_name
        self.domains_index = list(range(len(self.provider.domains_name)))

        self.nn_classes_size = len(self.provider.classes_list)
        # self.valid_nn = [self.provider.nn_dictionary[x] for x in self.provider.nn_dictionary if len(self.provider.nn_entry_classes_mapping[x]) > 1]
        # self.nn_classes_size = {x: len(self.provider.nn_classes_list[x]) for x in self.valid_nn}
        self.all_words = self.provider.dictionary_list
        self.word_possible_classe_one_hot = np.asarray([self.one_hot(word) for word in self.all_words])

        # DEV optimizations
        self.dev_coordinates = []
        self.dev_id = []
        self.dev_words = []
        self.dev_pos = []
        self.dev_distr = []
        self.dev_sentences_id = []

        for doc, sen, ind in self.provider.to_disambiguate_words_multi_sense_words_with_embeddings[1]:
            word = self.provider.loader.doc_sentences[DEV][WORD][doc][sen][ind]
            pos = self.provider.loader.doc_sentences[DEV][POS][doc][sen][ind]
            idd = self.provider.loader.doc_sentences[DEV][ID][doc][sen][ind]
            distr = self.batcher.get_domain_distribution(DEV, doc, sen)

            self.dev_coordinates.append((doc, sen, ind))
            self.dev_id.append(idd)
            self.dev_words.append(self.provider.dictionary[word])
            self.dev_pos.append(self.provider.poses_dictionary[pos])
            self.dev_distr.append(distr)
            self.dev_sentences_id.append(self.provider.docsentence2sentence[(1, doc, sen)])

        # TEST optimizations
        self.test_coordinates = []
        self.test_id = []
        self.test_words = []
        self.test_pos = []
        self.test_distr = []
        self.test_sentences_id = []
        for doc, sen, ind in self.provider.to_disambiguate_words_multi_sense_words_with_embeddings[2]:
            word = self.provider.loader.doc_sentences[TEST][WORD][doc][sen][ind]
            pos = self.provider.loader.doc_sentences[TEST][POS][doc][sen][ind]
            idd = self.provider.loader.doc_sentences[TEST][ID][doc][sen][ind]
            distr = self.batcher.get_domain_distribution(TEST, doc, sen)

            self.test_coordinates.append((doc, sen, ind))
            self.test_id.append(idd)
            self.test_words.append(self.provider.dictionary[word])
            self.test_pos.append(self.provider.poses_dictionary[pos])
            self.test_distr.append(distr)
            self.test_sentences_id.append(self.provider.docsentence2sentence[(2, doc, sen)])

    def one_hot(self, word):
        """
        Perform the one-hot encoding of the possible senses of a given word.
        :param word: the word
        """
        one_hot_array = np.zeros(self.nn_classes_size)
        if not word in self.provider.loader.word2synsets[TRAIN]:
            return one_hot_array

        classes = self.provider.loader.word2synsets[TRAIN][word].keys()
        for c in classes:
            if c not in self.provider.classes_dictionary:
                continue
            index = self.provider.classes_dictionary[c]
            one_hot_array[index] = 1
        return one_hot_array

    def print_debug(self, x_train, domain_distribution, words):
        """
        Prints some information about the sampled words
        :param x_train:  the probability distribution of the sentence (ground truth)
        :param domain_distribution: the domain distribution resulting from the multinomial in tf
        :param words: the sampled words in tf
        """
        for proba, (i_res, res), b in zip(x_train, enumerate(domain_distribution), words):
            topn = domain_classifier.Classifier.print_top_n_predictions(self.domains_name, proba, 5)
            topn = [x for x, _ in topn]
            print(res)
            for i_domain, domain in enumerate(self.domains_name):
                if domain in topn:

                    c = Counter(res)
                    print("{}\n\tOriginal distribution: {}\n\tDistribution resulting from the multinomial: {} ({}/{})\n".format(domain,
                                                        x_train[i_res][i_domain],
                                                        c[i_domain] / SAMPLE_SIZE, c[i_domain], len(res)))
            print(np.asarray(list(self.provider.reverse_dictionary[x] for x in b)))
            print()

    def train_and_predict(self):
        with tf.name_scope('embeddings'):
            #  the variable containing the google embeddings
            embeddings = tf.Variable(np.asarray(self.provider.embeddings_list_used), trainable=False, name="google",
                                     dtype=tf.float32)

        with tf.name_scope('semcor'):
            #  the variables containing the sentences, the mask and the average of the words in each sentence
            # semcor_sentences = tf.Variable(np.asarray(self.semcor_data), trainable=False, name="sentences", dtype=tf.int32)
            # semcor_sentences_mask = tf.Variable(np.asarray(self.semcor_mask), trainable=False, name="mask", dtype=tf.bool)
            semcor_sentences_avg = tf.Variable(np.asarray(self.semcor_average_sentence),
                                               expected_shape=(len(self.semcor_average_sentence), EMBEDDINGS_DIM),
                                               trainable=False, name="avg_sentences", dtype=tf.float32)

        with tf.name_scope('domains'):
            #  variables containing the words domain, their mask and the size of each domain.
            domain_words = tf.Variable(np.asarray(self.words_in_domain), trainable=False, name="words", dtype=tf.int32)
            domain_words_mask = tf.Variable(np.asarray(self.words_in_domain_mask), trainable=False, name="mask",
                                            dtype=tf.int32)
            domains_size = tf.reduce_sum(tf.cast(domain_words_mask, tf.int32), axis=1, name="sizes")

        # with tf.name_scope('classes'):
        #  variables containing a one_hot encoding of the possible classes of each word.
        # word2possible_classes_init = tf.placeholder(dtype=tf.bool,
        #                                             shape=(len(self.all_words), self.nn_classes_size))
        # word2possible_classes = tf.Variable(self.word_possible_classe_one_hot, trainable=False, name="possible_classes_one_hot", dtype=tf.boo)
        # word2possible_classes = tf.Variable(word2possible_classes_init)

        with tf.name_scope('inputs'):
            #  the placeholders to feed data into the network
            x_train_data_domain_distribution = tf.placeholder(dtype=tf.float32, shape=[None, self.number_domains],
                                                              name="train_data_domain_distribution")
            x_train_data_pos = tf.placeholder(dtype=tf.int32, shape=[None], name="train_data_pos")
            x_train_data_words = tf.placeholder(dtype=tf.int32, shape=[None], name="train_data_words")
            y_train_classes = tf.placeholder(dtype=tf.int32, shape=[None], name="y_train")

            prediction_sentence_id = tf.placeholder(dtype=tf.int32, shape=[None], name="sentence_ids")

        with tf.name_scope('sampling_domain_words'):
            #  operations to perform the sampling of the domain words according to the sentence domain distribution

            #  log probabilities must be used
            log_prob = tf.log(x_train_data_domain_distribution)

            #  get the number of samples that must be done in each domain
            domain_distribution = tf.multinomial(log_prob, SAMPLE_SIZE, output_dtype=tf.int32)

            def _uniform_sampling(single_multinomial):
                """
                for each number in the domain_distributino, perform a random sampling in the correspondent domain.
                :param single_multinomial: a list of integer representing domains. For each integer a word must be sampled from that domain
                :return: the sampled words
                """
                return tf.map_fn(
                    lambda x: domain_words[x, tf.random_uniform((), maxval=domains_size[x], dtype=tf.int32)],
                    single_multinomial, tf.int32)

            # for each sample in the batch, apply the function uniform_sampling
            word_samples = tf.map_fn(lambda x: _uniform_sampling(x), domain_distribution, dtype=tf.int32)

        with tf.name_scope('average_domain_direction'):
            #  take the average of the sampled words
            word_embeddings = tf.nn.embedding_lookup(embeddings, word_samples)
            words_average = tf.reduce_mean(word_embeddings, [1])

        with tf.name_scope("preprocessing"):
            #  the input is made of:

            # embedding of the word to disambiguate
            embds = tf.nn.embedding_lookup(embeddings, x_train_data_words)

            # POS of the word to disambiguate
            pos_one_hot = tf.one_hot(x_train_data_pos, len(self.provider.poses), axis=1, dtype=tf.float32)

            # average of the sample words, concatenated togheter.
            input_concatenation = tf.concat([embds, pos_one_hot, words_average], axis=1)

            #  define another possible input, as the concatenation of the word, the pos and the avg of the sentence
            batch_average_sentence = tf.gather(semcor_sentences_avg, prediction_sentence_id)
            input_concatenation_test = tf.concat([embds, pos_one_hot, batch_average_sentence], axis=1)

        with tf.name_scope("train_netowork"):
            weights = {
                'h1': tf.Variable(tf.random_normal([INPUT, HIDDEN_LAYER_1], dtype=tf.float32), dtype=tf.float32),
                'h2': tf.Variable(tf.random_normal([HIDDEN_LAYER_1, HIDDEN_LAYER_2], dtype=tf.float32),
                                  dtype=tf.float32),
                'out': tf.Variable(tf.random_normal([HIDDEN_LAYER_2, self.nn_classes_size], dtype=tf.float32),
                                   dtype=tf.float32)
            }

            biases = {
                'b1': tf.Variable(tf.random_normal([HIDDEN_LAYER_1], dtype=tf.float32), dtype=tf.float32),
                'b2': tf.Variable(tf.random_normal([HIDDEN_LAYER_2], dtype=tf.float32), dtype=tf.float32),
                'out': tf.Variable(tf.random_normal([self.nn_classes_size], dtype=tf.float32), dtype=tf.float32)
            }

            # the first layer with the activation
            layer_1 = tf.add(tf.matmul(input_concatenation, weights['h1']), biases['b1'])
            layer_1 = tf.nn.tanh(layer_1)

            # the second layer with the activation
            layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
            layer_2 = tf.nn.tanh(layer_2)

            # the output layer
            pred = tf.matmul(layer_2, weights['out']) + biases['out']

        with tf.name_scope("test_network"):
            #  build another identical netowork but instead of using the average of the sample word, uses the average of the sentence.

            # the first layer with the activation
            layer_1_test = tf.add(tf.matmul(input_concatenation_test, weights['h1']), biases['b1'])
            layer_1_test = tf.nn.tanh(layer_1_test)

            # the second layer with the activation
            layer_2_test = tf.add(tf.matmul(layer_1_test, weights['h2']), biases['b2'])
            layer_2_test = tf.nn.tanh(layer_2_test)

            # the output layer
            pred_test = tf.matmul(layer_2_test, weights['out']) + biases['out']

        with tf.name_scope("loss_calculation"):
            #  define the loss
            #
            # a tentative to compute the loss only on the possible senses of a word. Yelds worse performances.
            # pred_mask = tf.cast(tf.gather(word2possible_classes, x_train_data_words), dtype=tf.float32)
            # min_pred = tf.reduce_min(pred, axis=1)
            # pred = tf.add(pred, tf.reshape(tf.abs(min_pred), (-1, 1)))
            # pred = tf.multiply(pred, pred_mask)

            loss = tf.losses.sparse_softmax_cross_entropy(logits=pred, labels=y_train_classes)

            loss = tf.reduce_mean(loss)

        with tf.name_scope("optimizer"):
            # define the optimizer to use
            # opt = tf.train.AdagradOptimizer(learning_rate=LEARNING_RATE)
            opt = tf.train.AdamOptimizer(learning_rate=LEARNING_RATE)
            to_train = opt.minimize(loss)

        #  get the variable initializer
        init = tf.global_variables_initializer()

        #  manage the save and restore
        saver = tf.train.Saver()

        #  finalize the graph to avoid adding other nodes
        tf.get_default_graph().finalize()

        with tf.Session() as sess:
            if RESTORE_MODDEL:
                saver.restore(sess, TENSORFLOW_MODEL)
            else:
                sess.run(init)
                # sess.run(init, feed_dict={word2possible_classes_init: self.word_possible_classe_one_hot})

            if PERFOM_TRAINING:
                self.print_performance(sess, pred, pred_test, x_train_data_words, x_train_data_pos,
                                       x_train_data_domain_distribution, prediction_sentence_id)

                avg_loss = 0
                batch_number = 0
                for c in tqdm.tqdm(range(TRAINING_STEPS)):
                    targets, x_words, x_pos, x_distr, y_train, batch_number = self.batcher.create_batch(BATCH_SIZE,
                                                                                                        batch_number)

                    ws, ds, w, _, lo = sess.run([word_samples, domain_distribution, pred, to_train, loss], feed_dict={x_train_data_words: x_words,
                                                                           x_train_data_pos: x_pos,
                                                                           x_train_data_domain_distribution: x_distr,
                                                                           y_train_classes: y_train})

                    # self.print_debug(x_distr, ds, ws)

                    avg_loss += lo
                    if c % PRINT_STEPS == 0 and c:
                        print("current loss:", lo)
                        print("loss media:", avg_loss / c)
                        self.print_performance(sess, pred, pred_test, x_train_data_words, x_train_data_pos,
                                               x_train_data_domain_distribution, prediction_sentence_id)

            dev_predictions_both = self.print_performance(sess, pred, pred_test, x_train_data_words,
                                                              x_train_data_pos,
                                                              x_train_data_domain_distribution, prediction_sentence_id)
            Utils.save_json(dev_predictions_both, DEV_PREDICTIONS, True)

            # predict and save TEST set
            test_predictions = self.most_commont_prediction(self.predict_test, sess, pred, x_train_data_words,
                                                            x_train_data_pos,
                                                            x_train_data_domain_distribution, prediction_sentence_id)
            self.generate_results(test_predictions)

            if SAVE_MODEL:
                saver.save(sess, TENSORFLOW_MODEL)
                tf.summary.FileWriter(DEFAULT_TENSORBOARD_FOLDER, sess.graph)

    def print_performance(self, sess, pred, pred_test, x_train_data_words, x_train_data_pos,
                          x_train_data_domain_distribution, prediction_sentence_id):
        """
        Print the predictions on the DEV using the average of the sampled words and the average of the sentences.
        """
        print("Performances with domains word")
        most_common_pred = self.most_commont_prediction(self.predict_dev, sess, pred, x_train_data_words,
                                                        x_train_data_pos,
                                                        x_train_data_domain_distribution, prediction_sentence_id)
        self.evaluator.eval_by_semcor(most_common_pred)

        print("Performancese with avg sentence")
        avg_pred = self.predict_dev(sess, pred_test, x_train_data_words, x_train_data_pos,
                                    x_train_data_domain_distribution, prediction_sentence_id)
        self.evaluator.eval_by_semcor(avg_pred)
        return most_common_pred, avg_pred

    def most_commont_prediction(self, predictor, sess, tfpred, tword, tpos, tdom, tsen):
        """
        Performs PREDICTIONS_REPETITIONS predictioncs using predictor, then for each ID takes the most common.
        :param predictor: the predictor to use: self.predict_dev or self.predict_test
        :param sess: the session
        :param tfpred: the output layer of the network to use
        :param tword: the placeholder for the words
        :param tpos: the placeholder for the pos
        :param tdom: the placeholder for the domain
        :param tsen: the placeholder for the sentences id
        :return: the predictions
        """
        print("Predicting", predictor.__name__, "with the most common strategy")
        tot_pred = {}

        for x in range(PREDICTIONS_REPETITIONS):
            predictions = predictor(sess, tfpred, tword, tpos, tdom, tsen)
            tot_pred[x] = predictions

        finalpred = {}
        for x in tot_pred[0]:
            counter = Counter(tot_pred[i][x] for i in tot_pred)
            finalpred[x] = counter.most_common(1)[0][0]
        return finalpred

    def predict_dev(self, sess, tfpred, tword, tpos, tdom, tsen):
        """
        predict the synset of the words in the dev set
        :param sess: the session
        :param tfpred: the output layer of the network to use
        :param tword: the placeholder for the words
        :param tpos: the placeholder for the pos
        :param tdom: the placeholder for the domain
        :param tsen: the placeholder for the sentences id
        :return: the predictions
        """
        pred = sess.run(tfpred, feed_dict={tword: self.dev_words,
                                           tpos: self.dev_pos,
                                           tdom: self.dev_distr,
                                           tsen: self.dev_sentences_id})
        predictions = self.perform_predictions(pred, self.dev_words, self.dev_id, self.dev_pos, DEV)
        return predictions

    def predict_test(self, sess, tfpred, tword, tpos, tdom, tsen):
        """
        predict the synset of the words in the test set, and save the results to file
        :param sess: the session
        :param tfpred: the output layer of the network to use
        :param tword: the placeholder for the words
        :param tpos: the placeholder for the pos
        :param tdom: the placeholder for the domain
        :param tsen: the placeholder for the sentences id
        :return: the predictions
        """
        pred = sess.run(tfpred, feed_dict={tword: self.test_words,
                                           tpos: self.test_pos,
                                           tdom: self.test_distr,
                                           tsen: self.test_sentences_id})
        predictions = self.perform_predictions(pred, self.test_words, self.test_id, self.test_pos, TEST)
        return predictions

    def perform_predictions(self, pred, list_words, list_id, list_pos, dataset_name):
        """
        Given a the result of the network pred, predicts the class for each word
        :param pred: result of the nn
        :param list_words: list of words (numerical)
        :param list_id: list of the id (string) of the words
        :param list_pos: list of the pos (numerical) of the words
        :param dataset_name: for which dataset the predictions should be done (to autocomplete the missing ones using the mfs)
        :return: the predictions
        """
        predictions = {}
        for id_word, idd, pos, proba in zip(list_words, list_id, list_pos, pred):
            word = self.provider.reverse_dictionary[id_word]
            pos = self.provider.poses[pos]
            entry = (word, pos)

            assert entry in self.provider.loader.word_pos2synsets[TRAIN]

            possible_classes = [self.provider.classes_dictionary[i] for i in
                                self.provider.loader.word_pos2synsets[TRAIN][entry]]

            assert possible_classes and len(possible_classes) > 1
            max_class = max(possible_classes, key=lambda x: proba[x])

            predictions[idd] = self.provider.reverse_classes_dictionary[max_class]

        autocomplete = self.mfs.autocompolete_predictios(list_id, dataset_name)
        predictions.update(autocomplete)
        return predictions

    @staticmethod
    def generate_results(predictions):
        """
        Saves the predictions in a file
        :param predictions: the predictions to save
        """
        str_file = ""
        for pred in sorted(predictions):
            str_file += "{}\t{}\n".format(pred, predictions[pred])
        Utils.save_string(str_file, OUTPUT_TEST_FILE, True)


if __name__ == '__main__':
    WSD().train_and_predict()
