import Utils
import tqdm
import random

from Chronometer import Chrono
from wsd_loader import Loader, TRAIN, DEV, TEST, WORD, POS, LEMMA, ID
from babelnet_importer import BabelMap
from constants import *
from wsd_tensorflow_provider import Provider
import numpy as np


class Batch:
    """
    Manages the creation of the batches to train the neural network
    """

    def __init__(self, dataset_name=TRAIN):
        self.provider = Provider.get_instance()
        self.classifier = self.provider.classifier
        self.domain_predictions_cache = self._generate_predictions_for_datasets()

        self.dataset_name = dataset_name
        self.dataset_index = self.provider.dataset_names.index(dataset_name)

        self.number_of_domains = len(self.provider.domains_name)

        self.id2synset = self.provider.loader.id2synset[self.dataset_name]

        self.ambiguous_instances = self.provider.to_disambiguate_words_multi_sense_words_with_embeddings[self.dataset_index][:]
        random.shuffle(self.ambiguous_instances)

        self.number_of_instances = len(self.ambiguous_instances)

    def _generate_predictions_for_datasets(self, renew=False):
        """
        Generates, and caches, the predictions for the sentences in each dataset.

        :param renew: Whether the cache must be renewed
        """
        c = Chrono("Loading domain predictions for entire dataset...")
        if not renew:
            cache = Utils.load_pickle(CACHE_FILE_DATASET_DOMAIN_PREDICTIONS)
            if cache:
                c.millis("cached")
                return cache

        predictions = {x: {y: {} for y in [PRED, PROBA]} for x in self.provider.dataset_names}

        for x in self.provider.dataset_names:
            for i_doc, doc in tqdm.tqdm(enumerate(self.provider.loader.doc_sentences_word_restored[x]), total=len(self.provider.loader.doc_sentences_word_restored[x])):
                for i_sen, sentence in enumerate(doc):
                    pred, proba = self.classifier.predict_both(sentence)  #todo can be None if  if all the words in a sentence aren't in the google embeddings.
                    predictions[x][PRED][(x, i_doc, i_sen)] = pred
                    predictions[x][PROBA][(x, i_doc, i_sen)] = proba

        Utils.save_pickle(predictions, CACHE_FILE_DATASET_DOMAIN_PREDICTIONS, override=renew)
        c.millis("generated")
        return predictions

    def get_domain_distribution(self, dataset_name, idoc, isen):
        """
        Gets the domain distribution of a sentence

        :param dataset_name: the dataset of the sentence
        :param idoc: the index document of the sentence
        :param isen: the index of the sentence
        :return: the domain distribution of the sentence
        """
        entry = (dataset_name, idoc, isen)
        return self.domain_predictions_cache[dataset_name][PROBA][entry]

    def get_prediction(self, dataset_name, idoc, isen):
        """
        Get the prediction (a single domain) of a sentence.

        :param dataset_name: the dataset of the sentence
        :param idoc: the index document of the sentence
        :param isen: the index of the sentence
        :return: the predicted domain
        """
        entry = (dataset_name, idoc, isen)
        return self.domain_predictions_cache[dataset_name][PRED][entry]

    def create_batch(self, batch_size, i_instance):
        """
        Returns the next batch to be created

        :param batch_size: dimension of the batch
        :param i_instance: index from which the batch must start
        :return: the data batch and the final index
        """
        targets = np.empty(batch_size, dtype=(int, 4))
        # targets = [None]*batch_size
        x_train_word_identifier = np.empty(batch_size)
        x_train_pos_identifier = np.empty(batch_size)
        x_train_domain_distribution = np.empty(batch_size, dtype=(float, self.number_of_domains))
        y_train = np.empty(batch_size)

        index = 0
        while index < batch_size:
            target = self.ambiguous_instances[i_instance]
            i_doc, i_sen, i_word = target

            probability_distribution = self.get_domain_distribution(self.dataset_name, i_doc, i_sen)

            if probability_distribution is not None:  # it may not be available, if all the words in a sentence aren't in the google embeddings.
                idd = self.provider.loader.doc_sentences[self.dataset_name][ID][i_doc][i_sen][i_word]
                word = self.provider.loader.doc_sentences[self.dataset_name][WORD][i_doc][i_sen][i_word]
                pos = self.provider.loader.doc_sentences[self.dataset_name][POS][i_doc][i_sen][i_word]

                targets[index] = (self.dataset_index, i_doc, i_sen, i_word)
                # targets[index] = idd

                x_train_word_identifier[index] = self.provider.dictionary[self.provider.word2google_word[word]]
                x_train_pos_identifier[index] = self.provider.poses_dictionary[pos]

                x_train_domain_distribution[index] = probability_distribution
                y_train[index] = self.provider.classes_dictionary[self.id2synset[idd]]
                index += 1

            i_instance += 1

            if i_instance == self.number_of_instances:
                print("ALL DATA HAS BEEN USED!")
                i_instance = 0

        return targets, x_train_word_identifier, x_train_pos_identifier, x_train_domain_distribution, y_train, i_instance

    # def create_focused_batch(self, batch_size, i_instance, nn_id, suppress_warning=False):
    #     targets = [None] * batch_size
    #     x_train = [None] * batch_size
    #     y_train = [None] * batch_size
    #     index = 0
    #
    #     occurrences = self.provider.nn_id2occurences[nn_id]
    #     number_of_occurrences = len(occurrences)
    #
    #     if not suppress_warning and number_of_occurrences < 2:
    #         print("WARNING: predicting an ambiguos word with less than 2 classes")
    #
    #     while index < batch_size:
    #         target = occurrences[i_instance]
    #         i_doc, i_sen, i_word = target
    #
    #         idd = self.provider.loader.doc_sentences[self.dataset_name][ID][i_doc][i_sen][i_word]
    #         word = self.provider.loader.doc_sentences[self.dataset_name][WORD][i_doc][i_sen][i_word]
    #         pos = self.provider.loader.doc_sentences[self.dataset_name][POS][i_doc][i_sen][i_word]
    #         entry = (word, pos)
    #
    #         probability_distribution = self.get_domain_distribution(self.dataset_name, i_doc, i_sen)
    #
    #         if probability_distribution is not None:  # it may not be available, if all the words in a sentence aren't in the google embeddings.
    #             targets[index] = (self.dataset_index, i_doc, i_sen, i_word)
    #             x_train[index] = probability_distribution
    #             y_train[index] = self.provider.nn_entry_classes_mapping[entry][self.provider.loader.id2synset[self.dataset_name][idd]]
    #             index += 1
    #
    #         i_instance += 1
    #
    #         if i_instance == number_of_occurrences:
    #             i_instance = 0
    #
    #     return np.asarray(targets), np.asarray(x_train), np.asarray(y_train), np.asarray(i_instance)


if __name__ == '__main__':
    """
    Some testing for the performances
    """
    b = Batch()
    i = 0
    for _ in tqdm.tqdm(range(200000)):
        a, e, e1, e2, c, i = b.create_batch(16, i)
