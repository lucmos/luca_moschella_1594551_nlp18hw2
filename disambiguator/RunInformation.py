import Utils


class RunInfo:
    """
    Wrapper to information of a run of the domain classifier
    """
    def __init__(self, list_domains=None, name_domain2token_domain=None,
                 word2doc_frequency=None, domain2word2total_count=None, number_of_files=None):
        self.list_domains = list_domains
        self.name_domain2token_domain = name_domain2token_domain

        self.word2doc_frequency = word2doc_frequency
        self.domain2word2total_count = domain2word2total_count
        self.number_of_files = number_of_files
