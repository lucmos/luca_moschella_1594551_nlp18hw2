package json;

import java.util.HashMap;
import java.util.HashSet;

public class BabelnetDomains {
    public HashMap<String, HashSet<String>> domain2words;
//    int count;
//    int tot;
    public BabelnetDomains(){
        this.domain2words = new HashMap<>();
    }

    public void addWordInDomain(String domain, String word) {
        if (!this.domain2words.containsKey(domain)) {
            this.domain2words.put(domain, new HashSet<>());
        }
        this.domain2words.get(domain).add(word);
//        ++count;
//        ++tot;
//        if (count == 10000) {
//            count = 0;
//            System.out.println("TOTAL WORDS: " + tot);
//            for (String s : domain2words.keySet()) {
//                System.out.println(s + " - " + domain2words.getOrDefault(s, new HashSet<>()).size());
//            }
//            System.out.println();
//        }
    }

    @Override
    public String toString() {
        return this.domain2words.toString();
    }

}
