package json;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class BabelnetExportContext {
    public HashMap<String, HashMap<Integer, Set<String>>> domain_2_distance_2_context_words;

    public BabelnetExportContext() {
        this.domain_2_distance_2_context_words = new HashMap<>();
    }

    public void addContextWord(String domain, int distance, String context) {
        if (!this.domain_2_distance_2_context_words.containsKey(domain)) {
            this.domain_2_distance_2_context_words.put(domain, new HashMap<>());
        }

        if (!this.domain_2_distance_2_context_words.get(domain).containsKey(distance)) {
            this.domain_2_distance_2_context_words.get(domain).put(distance, new HashSet<>());
        }

        this.domain_2_distance_2_context_words.get(domain).get(distance).add(context);
    }
    @Override
    public String toString() {
        return this.domain_2_distance_2_context_words.toString();
    }
}
