package json;

import java.util.*;

public class BabelnetExportCommon {
    public HashMap<String, HashMap<String, HashMap<String, String>>> lemma_word_2_most_common_synset;


    public BabelnetExportCommon(){
        this.lemma_word_2_most_common_synset = new HashMap<>();
    }

    public void addCommonSynset(String word, String kb, String pos, String synset) {
        if (!this.lemma_word_2_most_common_synset.containsKey(word)) {
            this.lemma_word_2_most_common_synset.put(word, new HashMap<>());
        }
        if (!this.lemma_word_2_most_common_synset.get(word).containsKey(kb)) {
            this.lemma_word_2_most_common_synset.get(word).put(kb, new HashMap<>());
        }
        this.lemma_word_2_most_common_synset.get(word).get(kb).put(pos, synset);
    }

    @Override
    public String toString() {
        return this.lemma_word_2_most_common_synset.toString();
    }

}
