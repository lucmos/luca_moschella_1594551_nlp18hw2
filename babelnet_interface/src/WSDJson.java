import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import json.BabelnetExportCommon;
import json.PythonExport;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WSDJson {

    private static final String JSON_FILE_PYTHON = "./wsd/exported_python.json";

    public static PythonExport read() {
        try {
            FileReader reader = new FileReader(JSON_FILE_PYTHON);
            return new Gson().fromJson(new JsonReader(reader), PythonExport.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void write(Object obj, Class classe, String name) {
        try {
            FileWriter writer = new FileWriter(name);
            new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(obj, classe, writer);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
