public enum DomainMapping {

    ANIMALS(new String[]{"bn:00004222n"}),
    ART_ARCHITECTURE_AND_ARCHAEOLOGY(new String[]{"bn:00005927n", "bn:00005442n", "bn:00005391n"}),
    BIOLOGY(new String[]{"bn:00010543n"}),
    BUSINESS_ECONOMICS_AND_FINANCE(new String[]{"bn:00014137n", "bn:00029676n", "bn:00034524n"}),
    CHEMISTRY_AND_MINERALOGY(new String[]{"bn:00018123n", "bn:00055135n"}),
    COMPUTING(new String[]{"bn:00021494n"}),
    CULTURE_AND_SOCIETY(new String[]{"bn:01187943n", "bn:00072566n"}),
    EDUCATION(new String[]{"bn:00026980n"}),
    ENGINEERING_AND_TECHNOLOGY(new String[]{"bn:00005105n"}),
    FARMING(new String[]{"bn:00002096n"}),
    FOOD_AND_DRINK(new String[]{"bn:00035650n", "bn:00010183n"}),
    GAMES_AND_VIDEO_GAMES(new String[]{"bn:00037180n", "bn:00021477n"}),
    GEOGRAPHY_AND_PLACES(new String[]{"bn:00040163n", "bn:00062699n"}),
    GEOLOGY_AND_GEOPHYSICS(new String[]{"bn:00040170n", "bn:00040188n"}),
    HEALTH_AND_MEDICINE(new String[]{"bn:00043358n", "bn:00054126n", "bn:00054128n", "bn:00054133n"}),
    HERALDRY_HONORS_AND_VEXILLOLOGY(new String[]{"bn:00043773n", "bn:00000704n", "bn:00646030n"}),
    HISTORY(new String[]{"bn:00044268n"}),
    LANGUAGE_AND_LINGUISTICS(new String[]{"bn:00049910n", "bn:00051395n"}),
    LAW_AND_CRIME(new String[]{"bn:00048655n", "bn:00023807n"}),
    LITERATURE_AND_THEATRE(new String[]{"bn:00051538n", "bn:00028609n"}),
    MATHEMATICS(new String[]{"bn:00053823n"}),
    MEDIA(new String[]{"bn:00054149n", "bn:00048459n"}),
    METEOROLOGY(new String[]{"bn:00054609n", "bn:00054608n"}),
    MUSIC(new String[]{"bn:00056443n"}),
    NUMISMATICS_AND_CURRENCIES(new String[]{"bn:00020500n", "bn:00024507n"}),
    PHILOSOPHY_AND_PSYCHOLOGY(new String[]{"bn:00061984n", "bn:00065026n"}),
    PHYSICS_AND_ASTRONOMY(new String[]{"bn:00056996n", "bn:00006663n"}),
    POLITICS_AND_GOVERNMENT(new String[]{"bn:00063351n", "bn:00041192n", "bn:00001426n", "bn:00007299n"}),
    RELIGION_MYSTICISM_AND_MYTHOLOGY(new String[]{"bn:00032768n", "bn:00056663n", "bn:00056674n"}),
    ROYALTY_AND_NOBILITY(new String[]{"bn:00055624n", "bn:00005659n"}),
    SPORT_AND_RECREATION(new String[]{"bn:00006759n", "bn:00027865n"}),
    TEXTILE_AND_CLOTHING(new String[]{"bn:00019980n", "bn:00006125n"}),
    TRANSPORT_AND_TRAVEL(new String[]{"bn:00022379n", "bn:00078085n"}),
    WARFARE_AND_DEFENSE(new String[]{"bn:00080439n", "bn:00025878n"});

    private String[] synsets;
    DomainMapping(String[] synsets) {
        this.synsets = synsets;
    }

    public String[] getSynsets() {
        return synsets;
    }
}
