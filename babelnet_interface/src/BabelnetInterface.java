import java.util.*;
import java.util.stream.Collectors;

import com.babelscape.util.UniversalPOS;
import it.uniroma1.lcl.babelnet.*;
import it.uniroma1.lcl.babelnet.data.BabelPointer;
import it.uniroma1.lcl.babelnet.data.BabelSenseSource;
import it.uniroma1.lcl.jlt.util.Language;
import it.uniroma1.lcl.kb.Domain;
import json.BabelnetDomains;
import json.BabelnetExportCommon;
import json.BabelnetExportContext;
import json.PythonExport;

public class BabelnetInterface {
    private static final int MAX_DISTANCE_ALL_POINTERS = 3;
    private static final int MAX_DISTANCE_WORDNET_POINTERS = 4;

    private static final String JSON_FILE_BABELNET_COMMON = "./wsd/exported_babelnet_common.json";
    private static final String JSON_FILE_BABELNET_ALL_POINTERS = "./wsd/exported_babelnet_all_pointers.json";
    private static final String JSON_FILE_BABELNET_WORDNET_PONTERS = "./wsd/exported_babelnet_wordnet_pointers.json";
    private static final String JSON_FILE_BABELNET_DOMAIN_WORDS = "./wsd/exported_babelnet_domain_words.json";

    private static BabelPointer[] all_pointers = new BabelPointer[]{
            BabelPointer.ALSO_SEE,
            BabelPointer.ANTONYM,
            BabelPointer.ATTRIBUTE,
            BabelPointer.CAUSE,
            BabelPointer.DERIVATIONALLY_RELATED,
            BabelPointer.GLOSS_DISAMBIGUATED,
            BabelPointer.GLOSS_MONOSEMOUS,

            BabelPointer.CAUSE,
            BabelPointer.ENTAILMENT,
            BabelPointer.ATTRIBUTE,

            BabelPointer.HOLONYM_MEMBER,
            BabelPointer.HOLONYM_PART,
            BabelPointer.HOLONYM_SUBSTANCE,

            BabelPointer.HYPERNYM,
            BabelPointer.HYPERNYM_INSTANCE,

            BabelPointer.HYPONYM,
            BabelPointer.HYPONYM_INSTANCE,

            BabelPointer.MERONYM_MEMBER,
            BabelPointer.MERONYM_PART,
            BabelPointer.MERONYM_SUBSTANCE,

            BabelPointer.PARTICIPLE,
            BabelPointer.PERTAINYM,
            BabelPointer.REGION,
            BabelPointer.REGION_MEMBER,
            BabelPointer.SIMILAR_TO,
            BabelPointer.TOPIC,
            BabelPointer.TOPIC_MEMBER,
            BabelPointer.USAGE,
            BabelPointer.USAGE_MEMBER,
            BabelPointer.VERB_GROUP,

            BabelPointer.WIKIDATA_HYPERNYM,
            BabelPointer.WIKIDATA_HYPONYM,
            BabelPointer.WIKIDATA_HYPONYM_INSTANCE,
            BabelPointer.WIKIDATA_MERONYM

    };

    private static BabelPointer[] wordnet_pointers = new BabelPointer[]{
            BabelPointer.ALSO_SEE,
            BabelPointer.ANTONYM,
            BabelPointer.ATTRIBUTE,
            BabelPointer.CAUSE,
            BabelPointer.DERIVATIONALLY_RELATED,
            BabelPointer.GLOSS_DISAMBIGUATED,
            BabelPointer.GLOSS_MONOSEMOUS,

            BabelPointer.CAUSE,
            BabelPointer.ENTAILMENT,
            BabelPointer.ATTRIBUTE,

            BabelPointer.HOLONYM_MEMBER,
            BabelPointer.HOLONYM_PART,
            BabelPointer.HOLONYM_SUBSTANCE,

            BabelPointer.HYPERNYM,
            BabelPointer.HYPERNYM_INSTANCE,

            BabelPointer.HYPONYM,
            BabelPointer.HYPONYM_INSTANCE,

            BabelPointer.MERONYM_MEMBER,
            BabelPointer.MERONYM_PART,
            BabelPointer.MERONYM_SUBSTANCE,

            BabelPointer.PARTICIPLE,
            BabelPointer.PERTAINYM,
            BabelPointer.REGION,
            BabelPointer.REGION_MEMBER,
            BabelPointer.SIMILAR_TO,
            BabelPointer.TOPIC,
            BabelPointer.TOPIC_MEMBER,
            BabelPointer.USAGE,
            BabelPointer.USAGE_MEMBER,
            BabelPointer.VERB_GROUP,
    };

    private static UniversalPOS[] POS = new UniversalPOS[]{
            UniversalPOS.NOUN,
            UniversalPOS.VERB,
            UniversalPOS.ADJ,
            UniversalPOS.ADV
    };

    private static final String BABELNET = "babelnet";
    private static final String BABELNET_ALL = "all_babelnet";
    private static final String WORDNET = "wordnet";

    public static void compute_common_synsets(BabelNet bn, PythonExport python_json, BabelnetExportCommon babelnetExportCommon) {
        for (String w : python_json.lemma_word_2_most_common_synset.keySet()) {
            BabelNetQuery query = new BabelNetQuery.Builder(w).from(Language.EN).source(BabelSenseSource.WN).build();
            List<BabelSynset> synsets = bn.getSynsets(query);
            if (!synsets.isEmpty()) {
                BabelSynset syn = synsets.stream().sorted(new BabelSynsetComparator(w, Language.EN)).findFirst().get();
                babelnetExportCommon.addCommonSynset(w, BABELNET, "ALL", syn.getID().toString());
            }

            query = new BabelNetQuery.Builder(w).from(Language.EN).build();
            synsets = bn.getSynsets(query);
            if (!synsets.isEmpty()) {
                BabelSynset syn = synsets.stream().sorted(new BabelSynsetComparator(w, Language.EN)).findFirst().get();
                babelnetExportCommon.addCommonSynset(w, BABELNET_ALL, "ALL", syn.getID().toString());
            }

            for (UniversalPOS p : POS) {
                query = new BabelNetQuery.Builder(w).from(Language.EN).source(BabelSenseSource.WN).POS(p).build();
                synsets = bn.getSynsets(query);

                if (!synsets.isEmpty()) {
                    BabelSynset syn = synsets.stream().sorted(new BabelSynsetComparator(w, Language.EN)).findFirst().get();
                    babelnetExportCommon.addCommonSynset(w, BABELNET, p.name(), syn.getID().toString());
                }

                query = new BabelNetQuery.Builder(w).from(Language.EN).POS(p).build();
                synsets = bn.getSynsets(query);
                if (!synsets.isEmpty()) {
                    BabelSynset syn = synsets.stream().sorted(new BabelSynsetComparator(w, Language.EN)).findFirst().get();
                    babelnetExportCommon.addCommonSynset(w, BABELNET_ALL, p.name(), syn.getID().toString());
                }


                String wn_id = python_json.lemma_word_2_most_common_synset.get(w).get(WORDNET).get(p.name());
                if (wn_id != null) {
                    BabelSynset bn_id = bn.getSynset(new WordNetSynsetID(wn_id));
                    babelnetExportCommon.addCommonSynset(w, WORDNET, p.name(), bn_id.getID().toString());
                }

            }
        }
    }

    private static void compute_domains_words(BabelNet bn, BabelnetExportContext babelnetExportContext, String domain, Collection<String> synsetsID, int distance, Set<String> explored, BabelPointer[] pointers_to_use, int max_distance) {
        if (distance > max_distance || synsetsID.isEmpty()) {
            return;
        }
        System.out.print("Computing distance " + distance + "...");

        Set<String> nextIteration = new HashSet<>();
        for (String synID : synsetsID) {
            if (!explored.contains(synID)) {
                explored.add(synID);

                BabelSynset by = bn.getSynset(new BabelSynsetID(synID));

                for (BabelSense bs : by.getSenses(Language.EN)) {
                    String lemma = bs.getSimpleLemma();
                    if (!explored.contains(lemma)) {
                        babelnetExportContext.addContextWord(domain, distance, lemma);
                        explored.add(lemma);
                    }
                }

                if (distance + 1 <= max_distance) {
                    Set<String> neighbours = by.getOutgoingEdges(pointers_to_use).stream()
                            .filter(e -> isFromWordNet(e.getBabelSynsetIDTarget().getID()) && !explored.contains(e.getBabelSynsetIDTarget().getID()))
                            .map(n -> n.getBabelSynsetIDTarget().getID())
                            .collect(Collectors.toSet());

                    nextIteration.addAll(neighbours);
                }
            }
        }

        System.out.println("OK");
        compute_domains_words(bn, babelnetExportContext, domain, nextIteration, distance + 1, explored, pointers_to_use, max_distance);
    }

    private static void all_compute_domains_words_with_pointers(BabelNet bn, BabelnetExportContext babelnetExportPointers, BabelPointer[] pointers, int max_distance) {
        for (DomainMapping domain : DomainMapping.values()) {
            System.out.println("COMPUTING: " + domain);
            compute_domains_words(bn, babelnetExportPointers, domain.name(), Arrays.asList(domain.getSynsets()), 0, new HashSet<>(), pointers, max_distance);
            System.out.println("terminated: " + domain.name());
            System.out.println();
        }
    }

    static private boolean isFromWordNet(String id) {
        return id.startsWith("bn:000") || id.startsWith("bn:0010") || id.startsWith("bn:0011");
    }

    private static void compute_domains_words_from_domain_importance(BabelNet bn, BabelnetDomains data) {
        bn.offsetStream()
                .filter(BabelnetInterface::isFromWordNet)
                .forEach(id ->
                        {
                            BabelSynset by = bn.getSynset(new BabelSynsetID(id));
                            HashMap<Domain, Double> domains_importance = by.getDomains();
                            if (domains_importance.isEmpty()) {
                                return;
                            }

                            for (BabelSense bs : by.getSenses(Language.EN, BabelSenseSource.WN)) {
                                for (Domain bd : domains_importance.keySet()) {
                                    data.addWordInDomain(bd.toString(), bs.getSimpleLemma());
                                }
                            }
                        }
                );
    }

    public static void main(String[] args) {
        PythonExport python_json = WSDJson.read();
        assert python_json != null;
        BabelNet bn = BabelNet.getInstance();

        BabelnetDomains domain_export = new BabelnetDomains();
        System.out.println("Computing words in babelnet domain...");
        compute_domains_words_from_domain_importance(bn, domain_export);
        WSDJson.write(domain_export, BabelnetDomains.class, JSON_FILE_BABELNET_DOMAIN_WORDS);
        System.out.println("DOMAINS OK");

//        BabelnetExportCommon babelnetExportCommon = new BabelnetExportCommon();
//        System.out.print("Computing common synsets...");
//        compute_common_synsets(bn, python_json, babelnetExportCommon);
//        WSDJson.write(babelnetExportCommon, babelnetExportCommon.getClass(), JSON_FILE_BABELNET_COMMON);
//        System.out.println("COMMONS OK");

//        BabelnetExportContext babelnetExportWordnetPointers = new BabelnetExportContext();
//        System.out.println("COMPUTING CONTEXT FOR WORDNET POINTERS");
//        all_compute_domains_words_with_pointers(bn, babelnetExportWordnetPointers, wordnet_pointers, MAX_DISTANCE_WORDNET_POINTERS);
//        WSDJson.write(babelnetExportWordnetPointers, babelnetExportWordnetPointers.getClass(), JSON_FILE_BABELNET_WORDNET_PONTERS);
//        System.out.println("WORDNET POINTERS OK");


//        BabelnetExportContext babelnetExportAllPointers = new BabelnetExportContext();
//        System.out.println("COMPUTING CONTEXT FOR ALL POINTERS");
//        all_compute_domains_words_with_pointers(bn, babelnetExportAllPointers, all_pointers, MAX_DISTANCE_ALL_POINTERS);
//        WSDJson.write(babelnetExportAllPointers, babelnetExportAllPointers.getClass(), JSON_FILE_BABELNET_ALL_POINTERS);
//        System.out.println("ALL POINTERS OK");

//        ####################### REFERENCE ##########################

//        BabelSynset by = bn.getSynset(new BabelSynsetID("bn:00080439n"));
//
//        int counter = 0;
//
//        for (BabelSynsetRelation edge : by.getOutgoingEdges()) {
//            if (isFromWordNet(edge.getBabelSynsetIDTarget().getID())) {
//
//                System.out.println(by.getID() + "\t" + by.getMainSense(Language.EN).get().getFullLemma() + " - " + edge.getPointer() + " - " + bn.getSynset(edge.getBabelSynsetIDTarget()).getMainSensePreferrablyIn(Language.EN).get().getSimpleLemma());
//                counter += 1;
//            }
//
//        }
//        System.out.println(by.getOutgoingEdges().size());
//        System.out.println(counter);
//        List<BabelSense> bs = by.getSenses(Language.EN);
//        List<BabelSense> senses = bn.getSensesFrom(by);
//
//        for (BabelSense x : bs) {
//
//            System.out.println(x.getSimpleLemma());
//        }

        // get all the senses (i.e. words within synsets)
        // for the corresponding word and language
//        for (BabelSense s : bn.getSensesContaining("house", Language.EN))
//            System.out.println(s);

        // get all the synsets containing "house" in English
        // sorts them by relevance and find the first one
//        List<BabelSynset> synsets = bn.getSynsets("youngishyoungish", Language.EN);
//        BabelSynset syn = null;
//        if( ! synsets.isEmpty() )
//        {
//            syn = synsets.stream().sorted(new BabelSynsetComparator("youngishyoungish", Language.EN)).findFirst().get();
//        }
//
////
////
//        System.out.println(syn);
//
//        // same for "piano" in Italian
//        List<BabelSynset> syn2 = bn.getSynsets("piano", Language.IT).stream()
//                .sorted(new BabelSynsetComparator("piano", Language.IT))
//                .collect(Collectors.toList());
//
//        // get the synset ID
//        System.out.println(syn.getID());
//        // get a standard printout of the synset
//        System.out.println(syn2);
//        // get the main sense of a synset
//        System.out.println(syn.getMainSense());
//        // get the domains of the synset
//        System.out.println(syn.getDomains());
//
//        // get a synset from its ID
//        BabelSynset entitySynset = bn.getSynset(new BabelSynsetID("bn:00031027n"));
//        System.out.println(entitySynset);
//        // definitions
//        System.out.println(entitySynset.getGlosses());
//        // Wikipedia categories
//        System.out.println(entitySynset.getCategories());
//        // senses in Italian
//        System.out.println(entitySynset.getSenses(Language.IT));
//
//        // get all the WordNet synsets
//        // and their neighbours
//        // **NOTE**: takes tens of minutes to run (as it iterates over millions of nodes)!!!
//		/*
//		bn.stream()
//			.filter(s -> s.getSenseSources().contains(BabelSenseSource.WN))
//			.forEach(bs ->
//				{
//					String neighbours = bs.getOutgoingEdges().stream().map(n -> n.getBabelSynsetIDTarget().getID()).collect(Collectors.joining());
//					System.out.println(bs.getID()+" -> "+neighbours);
//				});
//		 */
//
        // get all the WordNet synset offsets
        // and their neighbours
        // **NOTE**: takes several minutes to run BUT FASTER THAN ITERATING ON SYNSETS AS ABOVE
//        bn.offsetStream()
//                .filter(id -> isFromWordNet(id))
//                .forEach(id ->
//                {
//                    BabelSynset bs = bn.getSynset(new BabelSynsetID(id));
//                    String neighbours = bs.getOutgoingEdges().stream()
//                            .filter(e -> isFromWordNet(e.getBabelSynsetIDTarget().getID()))
//                            .map(n -> n.getBabelSynsetIDTarget().getID()).collect(Collectors.joining());
//                    System.out.println(bs.getID()+" -> "+neighbours);
//                });
    }
}